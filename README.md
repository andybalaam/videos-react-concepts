# React Concepts

Slides for some videos on [React](https://reactjs.org/).

The finished slides are at:

* [React Concepts](http://artificialworlds.net/presentations/react-concepts/react-concepts.html)
* [Using Redux to manage state in React](http://artificialworlds.net/presentations/react-redux/react-redux.html)
