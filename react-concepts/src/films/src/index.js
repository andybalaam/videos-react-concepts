import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Films from './Films';
ReactDOM.render(<Films />, document.getElementById('root'));
