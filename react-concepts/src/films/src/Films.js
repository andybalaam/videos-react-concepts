import React, { useState } from 'react';
import './Films.css';
import Title from './Title.js';
import Search from './Search.js';
import List from './List.js';

export default function Films() {
    const [ search, setSearch ] = useState("");
    const searchChanged = (e) => {
        setSearch(e.target.value);
        console.log(`Pretending to search: ${e.target.value}`);
    };
    return (
        <div className="Films">
            <Title />
            <Search search={search} searchChanged={searchChanged}/>
            <List highlight={search}/>
        </div>
    );
}
