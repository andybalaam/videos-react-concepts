import React from 'react';
import './List.css';

export default function List(props) {
    const films = [
        {
            "name": "The Matrix",
            "genre": "Sci-Fi",
            "loveorhate": "Love",
            "reasoning": "Coolness"
        },
        {
            "name": "2012",
            "genre": "Disaster",
            "loveorhate": "Hate",
            "reasoning": "Where do I begin?"
        },
        {
            "name": "The Godfather",
            "genre": "Crime",
            "loveorhate": "Love",
            "reasoning": "Characters"
        },

        {
            "name": "Inception",
            "genre": "Sci-Fi",
            "loveorhate": "Love",
            "reasoning": "Depth"
        },
        {
            "name": "Interstellar",
            "genre": "Sci-Fi",
            "loveorhate": "Hate",
            "reasoning": "Boring"
        },
        {
            "name": "Léon",
            "genre": "Crime",
            "loveorhate": "Love",
            "reasoning": "Honour"
        },
        {
            "name": "Terminator 2",
            "genre": "Sci-Fi",
            "loveorhate": "Love",
            "reasoning": "Arnie"
        },
        {
            "name": "Alien",
            "genre": "Horror",
            "loveorhate": "Love",
            "reasoning": "Style"
        },
        {
            "name": "WALL-E",
            "genre": "Children's",
            "loveorhate": "Love",
            "reasoning": "Empathy"
        },
        {
            "name": "Fargo",
            "genre": "Comedy",
            "loveorhate": "Love",
            "reasoning": "Ya"
        }
    ];
    const highlight = (hl, name) => {
        let i = name.indexOf(hl);
        if ( i < 0 ) {
            return <td>{name}</td>;
        } else {
            let ch1 = name.substring(0, i);
            let ch2 = name.substring(i, i + hl.length);
            let ch3 = name.substring(i + hl.length);
            return <td>{ch1}<span class="highlight">{ch2}</span>{ch3}</td>
        }
    };
    return (
        <div className="List">
        <table>
            <thead>
                <tr>
                    <th>Film</th>
                    <th>Genre</th>
                    <th>Love or hate?</th>
                    <th>Reasoning</th>
                </tr>
            </thead>
            <tbody>
                {
                    films.map(f =>
                        <tr key={f.name}>
                            {highlight(props.highlight, f.name)}
                            <td>{f.genre}</td>
                            <td>{f.loveorhate}</td>
                            <td>{f.reasoning}</td>
                        </tr>
                    )
                }
            </tbody>
        </table>
        </div>
    );
}


