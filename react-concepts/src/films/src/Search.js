import React from 'react';
import './Search.css';

export default function Search(props) {
    const calcClass = () => {
        if (props.search.length > 3) {
            return "bad";
        } else {
            return "";
        }
    }
    return (
        <span className="Search">
            Search:
            <input
                value={props.search}
                onChange={props.searchChanged}
                className={calcClass()}
            />
        </span>
    );
}

